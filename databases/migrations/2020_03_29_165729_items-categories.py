from orator.migrations import Migration


class Items-categoties(Migration):

    def up(self):
        with self.schema.create('items-categoties') as table:
            table.increments('id')
            table.string('title')
            table.string('description')

    def down(self):
        """
        Revert the migrations.
        """
        pass
