from orator.migrations import Migration


class Items(Migration):

    def up(self):
        with self.schema.create('items') as table:
            table.increments('item-id') # id-номер предмета
            table.int('store-id').unsigned() # id-номер склада, которому пренадлежит данная номенклатура
            table.foreign('store-id').references('id').on('stores')
            table.int('category').unsigned() # id категории
            table.foreign('category').references('id').on('items-categories')
            table.json('item-transactions').default('')
            table.int('amount')
            table.enum('amount-type', ['kg', 'litres', 'square-meter', 'cubic-meter', 'piece'])
            table.json('product-keys')
            table.int('default-price')
            table.int('recomended-price').nullable()
            table.timestamps()

            # ЧЕЛОВЕЧНЫЕ ТИПЫ ДАННЫХ
            table.string('nomenclature') # текст названия номенклатуры
            table.text('description').default('') # описание
            table.json('images') # список изображений
            table.int('primary-image') # основное изображение



    def down(self):
        """
        Revert the migrations.
        """
        pass
